import axios from "axios";
import { AsyncStorage } from "react-native";

const client = axios.create({
    baseURL: "http://a2013aa2.ngrok.io"
});

client.interceptors.request.use(
    async config => {
        const token = await AsyncStorage.getItem("token");
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);

export default client;
