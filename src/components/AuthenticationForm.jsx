import React, { useState, useContext } from "react";
import { StyleSheet, Alert } from "react-native";
import { Text, Input, Button } from "react-native-elements";
import Spacer from "./Spacer";
import { Context as AuthContext } from "../contexts/AuthContext";

const AuthenticationForm = ({ headerText, errorMessage, submit, btnText }) => {
    const { clearErrorMessage } = useContext(AuthContext)

    const [email, setEmail] = useState("");

    const [password, setPassword] = useState("");

    const errorAlert = () => {
        Alert.alert(
            errorMessage,
            "",
            [
                {
                    text: "OK",
                    onPress: clearErrorMessage
                }
            ],
            { cancelable: false }
        );
    };

    return (
        <>
            {errorMessage ? errorAlert() : null}
            <Spacer>
                <Text h3>{headerText}</Text>
            </Spacer>
            <Input
                label="Email"
                value={email}
                onChangeText={setEmail}
                autoCapitalize="none"
                autoCorrect={false}
            />
            <Spacer />
            <Input
                secureTextEntry
                label="Password"
                value={password}
                onChangeText={setPassword}
                autoCapitalize="none"
                autoCorrect={false}
            />
            <Spacer>
                <Button
                    title={btnText}
                    onPress={() => submit({ email, password })}
                />
            </Spacer>
        </>
    );
};

const styles = StyleSheet.create({});

export default AuthenticationForm;
