import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { withNavigation } from "react-navigation";
import { Text } from "react-native-elements";
import Spacer from "./Spacer";

const NavLink = ({ linkMsg, linkTo, navigation }) => {
    return (
        <TouchableOpacity onPress={() => navigation.navigate(linkTo)}>
            <Spacer>
                <Text style={styles.link}>{linkMsg}</Text>
            </Spacer>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    link: {
        color: "blue"
    }
});

export default withNavigation(NavLink);
