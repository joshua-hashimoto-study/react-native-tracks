import createDataContext from "./createDataContext";
import {
    FETCH_TRACKS
} from "../consts";
import trackApi from '../api/trackServer'

const trackReducer = (state, action) => {
    switch (action.type) {
        case FETCH_TRACKS:
            return action.payload
        default:
            return state;
    }
};

const fetchTracks = dispatch => async () => {
    const res = await trackApi.get('/tracks')
    dispatch({ type: FETCH_TRACKS, payload: res.data })
};

const createTrack = dispatch => async (name, locations) => {
    await trackApi.post('/tracks', { name, locations })
};

export const { Provider, Context } = createDataContext(
    trackReducer,
    { fetchTracks, createTrack },
    []
);
