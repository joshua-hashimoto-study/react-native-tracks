import { AsyncStorage } from "react-native";
import createDataContext from './createDataContext'
import trackApi from '../api/trackServer'
import { ADD_ERROR, SIGNIN, CLEAR_ERROR_MESSAGE, SIGNOUT } from "../consts";
import { navigate } from "../navigationRef";

const authReducer = (state, action) => {
    switch (action.type) {
        case ADD_ERROR:
            return {
                ...state,
                errorMessage: action.payload
            }
        case SIGNIN:
            return {
                errorMessage: '',
                token: action.payload
            }  // ログインに成功した場合はerrorMessageをリセットしたいので...stateはやらずに純粋に新しいオブジェクトを返す
        case CLEAR_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: ''
            }
        case SIGNOUT:
            return {
                token: null,
                errorMessage: ''
            }
        default:
            return state
    }
}

const clearErrorMessage = dispatch => {
    return () => {
        dispatch({ type: CLEAR_ERROR_MESSAGE })
    }
}

const signup = dispatch => {
    return async ({ email, password }) => {
        try {
            const res = await trackApi.post('/signup', { email, password })
            await AsyncStorage.setItem('token', res.data.token)
            dispatch({ type: SIGNIN, payload: res.data.token })
            navigate('TrackList')
        } catch (error) {
            dispatch({ type: ADD_ERROR, payload: 'Something went wrong!' })
        }
    }
}

const signin = dispatch => {
    return async ({ email, password }) => {
        try {
            const res = await trackApi.post('/signin', { email, password })
            await AsyncStorage.setItem('token', res.data.token)
            dispatch({ type: SIGNIN, payload: res.data.token })
            navigate('TrackList')
        } catch (error) {
            dispatch({ type: ADD_ERROR, payload: 'Your Email or Password is Wrong!' })
        }
    }
}

const signout = dispatch => {
    return async () => {
        await AsyncStorage.removeItem('token')
        dispatch({ type: SIGNOUT })
        navigate('loginFlow')
    }
}

const verifyToken = dispatch => {
    return async () => {
        const token = await AsyncStorage.getItem('token')
        if (token) {
            dispatch({ type: SIGNIN, payload: token })
            navigate('TrackList')
        } else {
            navigate('loginFlow')
        }
    }
}

export const { Provider, Context } = createDataContext(
    authReducer,
    { signin, signout, signup, clearErrorMessage, verifyToken },
    { token: null, errorMessage: '' }
)
