import createDataContext from "./createDataContext";
import {
    ADD_LOCATION,
    START_RECODING,
    STOP_RECODING,
    ADD_CURRENT_LOCATION,
    CHANGE_NAME,
    RESET
} from "../consts";

const locationReducer = (state, action) => {
    switch (action.type) {
        case ADD_CURRENT_LOCATION:
            return {
                ...state,
                currentLocation: action.payload
            };
        case START_RECODING:
            return {
                ...state,
                recording: true
            };
        case STOP_RECODING:
            return {
                ...state,
                recording: false
            };
        case ADD_LOCATION:
            return {
                ...state,
                locations: [...state.locations, action.payload]
            };
        case CHANGE_NAME:
            return {
                ...state,
                name: action.payload
            };
        case RESET:
            return {
                ...state,
                name: "",
                locations: []
            };
        default:
            return state;
    }
};

const startRecording = dispatch => () => {
    dispatch({ type: START_RECODING });
};

const stopRecording = dispatch => () => {
    dispatch({ type: STOP_RECODING });
};

const addLocation = dispatch => (location, recording) => {
    dispatch({ type: ADD_CURRENT_LOCATION, payload: location });
    if (recording) {
        dispatch({ type: ADD_LOCATION, payload: location });
    }
};

const changeName = dispatch => name => {
    dispatch({ type: CHANGE_NAME, payload: name });
};

const reset = dispatch => () => {
    dispatch({ type: RESET });
};

export const { Provider, Context } = createDataContext(
    locationReducer,
    { startRecording, stopRecording, addLocation, changeName, reset },
    { recording: false, locations: [], currentLocation: null, name: "" }
);
