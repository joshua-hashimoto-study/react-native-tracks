import React, { useContext } from "react";
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Keyboard
} from "react-native";
import { Context as AuthContext } from "../contexts/AuthContext";
import AuthenticationForm from "../components/AuthenticationForm";
import NavLink from "../components/NavLink";

const SignupScreen = () => {
    const { state, signup } = useContext(AuthContext);

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                <AuthenticationForm
                    headerText="Sign up for Tracker"
                    errorMessage={state.errorMessage}
                    submit={signup}
                    btnText="Sign Up"
                />

                <NavLink
                    linkMsg="Alreadry have an accounts? Sign in insread"
                    linkTo="Signin"
                />
            </View>
        </TouchableWithoutFeedback>
    );
};

SignupScreen.navigationOptions = () => {
    return {
        header: null
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        marginBottom: 250
    }
});

export default SignupScreen;
