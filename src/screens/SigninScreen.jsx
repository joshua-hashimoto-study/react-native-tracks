import React, { useContext, useEffect } from "react";
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Keyboard
} from "react-native";
// import { NavigationEvents } from "react-navigation";
import { Context as AuthContext } from "../contexts/AuthContext";
import AuthenticationForm from "../components/AuthenticationForm";
import NavLink from "../components/NavLink";

const SigninScreen = () => {
    const { state, signin } = useContext(AuthContext);

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                {/* <NavigationEvents onWillBlur={clearErrorMessage} /> */}
                <AuthenticationForm
                    headerText="Sign In to Your Account"
                    errorMessage={state.errorMessage}
                    submit={signin}
                    btnText="Sign In"
                />

                <NavLink
                    linkMsg="Don't have an account? Sign up insread"
                    linkTo="Signup"
                />
            </View>
        </TouchableWithoutFeedback>
    );
};

SigninScreen.navigationOptions = () => {
    return {
        header: null
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        marginBottom: 250
    }
});

export default SigninScreen;
