import React, { useEffect, useContext } from 'react'
import { View, Text } from 'react-native'
import { Context as AuthContext } from "../contexts/AuthContext";

const ResolveAuthScreen = () => {
    const { verifyToken } = useContext(AuthContext)
    
    useEffect(() => {
        verifyToken()
    }, [ ])

    return (
        null
    )
}

export default ResolveAuthScreen
