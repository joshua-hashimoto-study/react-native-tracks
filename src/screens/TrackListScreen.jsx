import React, { useContext } from 'react'
import { StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import { ListItem } from "react-native-elements";
import { NavigationEvents } from "react-navigation";
import { Context as TrackContext } from "../contexts/TrackContext";

const TrackListScreen = ({ navigation }) => {
    const { fetchTracks, state } = useContext(TrackContext)
    
    return (
        <>
            <NavigationEvents onWillFocus={fetchTracks}/>
            <FlatList
                data={state}
                keyExtractor={track => track._id}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity onPress={() => navigation.navigate('TrackDetail', { _id: item._id })}>
                            <ListItem chevron title={item.name} />
                        </TouchableOpacity>
                    )
                }}
            />
        </>
    )
}

TrackListScreen.navigationOptions = () => {
    return {
        title: 'My Tracks'
    }
}

const styles = StyleSheet.create({
    
})


export default TrackListScreen
